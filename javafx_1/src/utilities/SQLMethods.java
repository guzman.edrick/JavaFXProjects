package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SQLMethods {
	public Connection getDBConnection(){
		 FileInputStream fileInput;
		 Properties properties = new Properties();
		 Connection conn = null;
				try {
					try {
						fileInput = new FileInputStream("resources/config.properties");
						try {
							properties.load(fileInput);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();							
						}
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						
					}
					Class.forName(properties.getProperty("driver"));
					conn = DriverManager.getConnection(properties.getProperty("URL")+
							properties.getProperty("database"),
							properties.getProperty("username"),
							properties.getProperty("password"));
					
					
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			return conn;
			}
}
