package methods;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class MainControllerMethods {
	
	public String getFirstName(Connection conn)
	{
		String toReturn = "";
		try{
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT emp.firstName FROM emp WHERE id = 1");
			while(rs.next())
			{
			toReturn = rs.getString(1);
			}
			stmt.close();
			rs.close();
			return toReturn;
		}catch(Exception e){
			e.printStackTrace();
		}
		return toReturn;
		
	}

}
