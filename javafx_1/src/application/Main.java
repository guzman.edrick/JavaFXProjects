package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.Parent;




public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Screen screen = Screen.getPrimary();
			Rectangle2D bounds = screen.getVisualBounds();
//			System.out.println("Y: " + bounds.getMaxY() + " X: " + bounds.getMaxX());
//			System.out.println("Length: " + bounds.getWidth() + " Width: " + bounds.getHeight());
			primaryStage.setX(bounds.getMinX());
			primaryStage.setY(bounds.getMinY());
			primaryStage.setWidth(bounds.getWidth());
			primaryStage.setHeight(bounds.getHeight());
			Parent root = FXMLLoader.load(getClass().getResource("/application/main.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/stylesheet.css").toExternalForm());
			primaryStage.setTitle("hello");
			primaryStage.setScene(scene);
			primaryStage.show();
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
