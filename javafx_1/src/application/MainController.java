package application;

import javafx.event.ActionEvent;
import utilities.SQLMethods;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MainController {
	@FXML
	private Label myMessage;
	public void generateRandom(ActionEvent event){
		Random generator = new Random();
		int myRand = generator.nextInt(50)+1;
		System.out.println(Integer.toString(myRand));
		myMessage.setText(Integer.toString(myRand));	
	}
	
	public void callConnection(ActionEvent event){
		utilities.SQLMethods sql = new utilities.SQLMethods();
		methods.MainControllerMethods mcm = new methods.MainControllerMethods();
		Connection conn = sql.getDBConnection();
		myMessage.setText(mcm.getFirstName(conn));
		
	
		
	}

}
