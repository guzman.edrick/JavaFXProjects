/*
 * REMOVE FILE NAME FROM COORDINATES
 * 
 */




package javaprac;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class modelMaker {
	private static double eyeHeight;
	private static double eyeWidth;
	private static double mouthHeight;
	private static double mouthWidth;
	private static double eyebrowToEyeCenterHeight;
	private static double eyeCenterToMouthCenterHeight;
	private static double leftEyeCenterMouthTopLength;
	private static double leftEyeCenterMouthBottomLength;
	private static double rightEyeCenterMouthTopLength;
	private static double rightEyeCenterMouthBottomLength;
	private static double eyeWidthDivideEyeHeight;
	private static double mouthWidthDivideMouthHeight;
	private static double EyeEyeCenterDivideEyeEyeCenterPlusEyeMouthCenter;

	public static void main(String[] args){
		ArrayList<Double> xCoordinates = new ArrayList<Double>(); //List of all X Coordinates
		ArrayList<Double> yCoordinates = new ArrayList<Double>(); // List of all Y Coordinates
		ArrayList<String> trashList = new ArrayList<String>(); // All "true" dito mapupunta, nvm this
		
		String line = getCoordinates(); //get coordinates from text file
		
		String[] array = line.split(";"); // make into an array 
		
		
		for(int i = 0;i<=array.length-1;i += 3){
			xCoordinates.add(Double.parseDouble(array[i]));//populate xCoordinate
		}
		for(int j = 1;j<=array.length-1;j += 3){
			yCoordinates.add(Double.parseDouble(array[j]));//populate yCoordinate
		}
		for(int k = 2;k<=array.length-1;k += 3){
			trashList.add(array[k]);
		}
		

		double mouthMidPointX=getXMidpoint(xCoordinates.get(60), xCoordinates.get(57));
		double mouthMidPointY = getYMidpoint(yCoordinates.get(60), yCoordinates.get(57));
		eyeHeight = (getEuclideanDistance(xCoordinates.get(26),yCoordinates.get(26),xCoordinates.get(24),yCoordinates.get(24))+ 
				getEuclideanDistance(xCoordinates.get(31),yCoordinates.get(31),xCoordinates.get(29),yCoordinates.get(29)))/2;
		eyeWidth = (getEuclideanDistance(xCoordinates.get(25),yCoordinates.get(25),xCoordinates.get(24),yCoordinates.get(24))+ 
				getEuclideanDistance(xCoordinates.get(28),yCoordinates.get(28),xCoordinates.get(30),yCoordinates.get(30)))/2;
		mouthHeight = getEuclideanDistance(xCoordinates.get(47),yCoordinates.get(47),xCoordinates.get(50),yCoordinates.get(50));
		mouthWidth = getEuclideanDistance(xCoordinates.get(50),yCoordinates.get(50),xCoordinates.get(44),yCoordinates.get(44));
		eyebrowToEyeCenterHeight = (getEuclideanDistance(xCoordinates.get(27),yCoordinates.get(27),xCoordinates.get(21),yCoordinates.get(21))+ 
				getEuclideanDistance(xCoordinates.get(32),yCoordinates.get(32),xCoordinates.get(17),yCoordinates.get(17)))/2;
		eyeCenterToMouthCenterHeight = 	(getEuclideanDistance(mouthMidPointX,mouthMidPointY,xCoordinates.get(27),yCoordinates.get(27))+ 
				getEuclideanDistance(mouthMidPointX,mouthMidPointY,xCoordinates.get(32),yCoordinates.get(32)))/2;
		leftEyeCenterMouthTopLength = getEuclideanDistance(xCoordinates.get(27),yCoordinates.get(27),xCoordinates.get(47),yCoordinates.get(47));
		leftEyeCenterMouthBottomLength = getEuclideanDistance(xCoordinates.get(27),yCoordinates.get(27),xCoordinates.get(53),yCoordinates.get(53));
		rightEyeCenterMouthTopLength = getEuclideanDistance(xCoordinates.get(32),yCoordinates.get(32),xCoordinates.get(47),yCoordinates.get(47));
		rightEyeCenterMouthBottomLength = getEuclideanDistance(xCoordinates.get(32),yCoordinates.get(32),xCoordinates.get(53),yCoordinates.get(53));
		eyeWidthDivideEyeHeight = eyeWidth/eyeHeight;
		mouthWidthDivideMouthHeight = mouthWidth/mouthHeight;	
		EyeEyeCenterDivideEyeEyeCenterPlusEyeMouthCenter = eyebrowToEyeCenterHeight/(eyebrowToEyeCenterHeight+eyeCenterToMouthCenterHeight);

		StringBuilder sb = new StringBuilder();
				
		sb.append(eyeHeight+ "," + eyeWidth+ "," +mouthHeight+ "," +mouthWidth+ "," +eyebrowToEyeCenterHeight+ "," +eyeCenterToMouthCenterHeight+ "," +leftEyeCenterMouthTopLength+ "," +leftEyeCenterMouthBottomLength
		+ "," +rightEyeCenterMouthTopLength+ "," +rightEyeCenterMouthBottomLength+ "," +eyeWidthDivideEyeHeight+ "," +mouthWidthDivideMouthHeight+ "," +EyeEyeCenterDivideEyeEyeCenterPlusEyeMouthCenter);
		System.out.println(sb.toString());
	}
	
	public static double getYMidpoint(double y1, double y2){
		return (y1+y2)/2;
	}

	public static double getXMidpoint(double x1, double x2){
		return (x1+x2)/2;
	}
	public static double getEuclideanDistance(double x1,double y1, double x2, double y2)
	{
		  double y = Math.abs (y1 - y2);
		  double x = Math.abs (x1 - x2);
		  double distance = Math.sqrt((y)*(y) +(x)*(x));
		  return distance;
	}
	
	public static String getCoordinates()
	{
		 StringBuilder sb = new StringBuilder();
		 BufferedReader br = null;
		 String line = "";
	       try {
	           br = new BufferedReader(new FileReader("C:\\Users\\Edrick\\Desktop\\hello.txt"));
	           
	           while ((line = br.readLine()) != null) {
	        	   sb.append(line);
	               return line;
	           }
	           
	       } catch (IOException e) {
	           e.printStackTrace();
	       } finally {
	           try {
	               if (br != null) {
	                   br.close();
	               }
	           } catch (IOException ex) {
	               ex.printStackTrace();
	           }
	       }
	       line = sb.toString();
	       return line;
	}

}


