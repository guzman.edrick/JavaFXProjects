package prac;

import java.io.IOException;
import java.io.File; 
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

public class prac {

	public static void main(String[] args){
		File dir = new File("C:\\Users\\Edrick\\Desktop\\Java FX workspace\\Thesis\\src\\media");
		int numberOfSubfolders = 0;
		File listDir[] = dir.listFiles();
		for (int i = 0; i < listDir.length; i++) {
		    if (listDir[i].isDirectory()) {
		            numberOfSubfolders++;
		        }
		}
		System.out.println("No of dir " + numberOfSubfolders);
		
		
		int dir2 = new File("C:\\Users\\Edrick\\Desktop\\Java FX workspace\\Thesis\\src\\media").listFiles().length;
		System.out.println("No of files " + dir2);
		
		String source="C:\\Users\\Edrick\\Desktop\\Java FX workspace\\Thesis\\src\\media\\Greeting.mp4";
		String destination="C:\\Users\\Edrick\\Desktop\\Java FX workspace\\Thesis\\src\\media\\Dog\\1.mp4";

		try {
			
			copyFile_Java7(source,destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
      
	
	  public static void copyFile_Java7(String origin, String destination) throws IOException {
	        Path FROM = Paths.get(origin);
	        Path TO = Paths.get(destination);
	        //overwrite the destination file if it exists, and copy
	        // the file attributes, including the rwx permissions
	        CopyOption[] options = new CopyOption[]{
	          StandardCopyOption.REPLACE_EXISTING,
	          StandardCopyOption.COPY_ATTRIBUTES
	        }; 
	        Files.copy(FROM, TO, options);
			System.out.println("DONE");

	    }
}
