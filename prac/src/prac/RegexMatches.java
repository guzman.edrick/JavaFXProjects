package prac;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexMatches {
	
	public static void main(String[] args)
	{
		String line = "Frustrated 28-10-2016 12:15:34Calm 28-10-2016 12:15:36Calm 28-10-2016 12:15:38Calm 28-10-2016 12:15:40Engaged 28-10-2016 12:15:42";
		String pattern = "([a-zA-Z]*)|([0-9]{2}-[0-9]{2}-[0-9]{4})|([0-9]{2}:[0-9]{2}:[0-9]{2})";
		
		
		
		Timer t2 = new Timer();
		t2.schedule(new TimerTask() {
		    @Override
			    public void run() {
				String hello;
				ArrayList<String> emotion = new ArrayList<String>();
				ArrayList<String> date = new ArrayList<String>();
				ArrayList<String> time = new ArrayList<String>();
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				Date d1 = new Date();
				dateFormat.setTimeZone(TimeZone.getDefault());
				String emotionString = "";
				try {
					hello = hello();
					System.out.println(hello);
					String[] input = hello.split(" ");
					for(int i = 0;i<=input.length-1;i += 3){
						emotion.add(input[i]);
					}
					for(int j = 1;j<=input.length-1;j += 3){
						date.add(input[j]);
					}
					for(int k = 2;k<=input.length-1;k += 3){
						time.add(input[k]);
					}
					
						StringBuilder sb1 = new StringBuilder();
						log("{");
						for(int i = 0; i<=emotion.size()-1;i++){
							sb1.append(emotion.get(i) + " " + date.get(i) + " " + time.get(i));
							String toBeLogged = sb1.toString();
							log(toBeLogged);
							sb1 = new StringBuilder();
						}
						emotionString = whatToTrigger(emotion);
						log("} VIDEO TRIGGERED: " + emotionString + " " + dateFormat.format(d1));
						System.out.println("DONE");
						    
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
			}, 10000, 13000);
			
////			
////			for(int i = 0; i<=emotion.size()-1;i++){
////				System.out.println(emotion.get(i));
////			}
////			for(int i = 0; i<=date.size()-1;i++){
////				System.out.println(date.get(i));
////			}
////			for(int i = 0; i<=time.size()-1;i++){
////				System.out.println(time.get(i));
//			}
			
			
		
	

			
	}
	
	public static String whatToTrigger(ArrayList<String> emotion){
		String trigger = "";
		
		for(int i = 0;i<=emotion.size()-1;){
			trigger = emotion.get(i);
			if(trigger.equals("Bored") || trigger.equals("Frustrated")){
				return trigger;
			}
			else{
				trigger = "NOTHING TO TRIGGER";
				i++;
			}
			
		}
		return trigger;
	}
	public static String getFromTextFile()
	{
		 StringBuilder sb = new StringBuilder();
		 BufferedReader br = null;
		 String line = "";
	       try {
	           br = new BufferedReader(new FileReader("C:\\Users\\Edrick\\Desktop\\emotion.txt"));
	           
	           while ((line = br.readLine()) != null) {
	        	   sb.append(line);
	               return line;
	           }
	           
	       } catch (IOException e) {
	           e.printStackTrace();
	       } finally {
	           try {
	               if (br != null) {
	                   br.close();
	               }
	           } catch (IOException ex) {
	               ex.printStackTrace();
	           }
	       }
	       line = sb.toString();
	       return line;
	}
	
	public static String hello() throws FileNotFoundException
	{
			FileInputStream f = new FileInputStream("C:\\Users\\Edrick\\Desktop\\emotion.txt");

			InputStreamReader reader = new InputStreamReader(f);

			BufferedReader buff = new BufferedReader (reader);

			StringBuilder sb = new StringBuilder();

		
			
			StringBuffer stringBuffer = new StringBuffer("");
			// for reading one line
			String line = null;
			// keep reading till readLine returns null
			try {
				while ((line = buff.readLine()) != null) {
				    // keep appending last line read to buffer
				    stringBuffer.append(line);
				    sb.append(line+" ");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			line = sb.toString();
			line = line.substring(0, line.length()-1);
			return line;

	}
	
	public static void log(String toBeLogged){
		 Writer writer;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(
					        new FileOutputStream("src/prac/hello.txt", true), "UTF-8"));
				//writer = new PrintWriter("src/javaprac/hello.txt", "UTF-8");
				try {
					writer.append("\n" + toBeLogged);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				try {
					writer.close();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	


}
